package corejava;

public class EmployMain {

	public static void main(String[] args) {
		System.out.println("employ obj of the employ\n");
		Employ<Integer> e=new Employ<Integer>();
		//e.setId(10);
		//e.setAge(23);
		e.display();
		e.appraise();
		
		System.out.println("employ obj of the person\n");
		Person e1=new Employ(16,21,"ece",35000);
		e1.display();
		e1.sayHello();
		
		System.out.println("employ obj of the Appraisable\n");
		Appraisable e2=new Employ(17,22,"eee",45000);
		e2.appraise();
				
		System.out.println("contractor obj of the contractor\n");
        Cotractor c=new Cotractor(25,40000,3);
		c.display();
		
		System.out.println("contractor obj of the person\n");
        Person c1=new Cotractor(26,45000,5);
		c1.display();
	}
	
	public static void main() {
		System.out.println();
	}

}
