package corejava;

public abstract class Person {
   private int id;
   private int age;
   private String dept;
   private double salary;
   public Person()
   {
   	System.out.println("Person default constructor");
   }
   public Person(int id,int age){
   	this.id=id;
   	this.age=age;
   	
   }
   public void setId(int id) {
   	this.id=id;
   }
   public void setAge(int age) {
   	this.age=age;
   }
   public void setDept(String dept) {
   	this.dept=dept;
   }
   public void setSalary(double salary) {
   	this.salary=salary;
   }
   public int getId() {
   	return id;
   }
   public int getAge() {
   	return age;
   }
   public String getDept() {
   	return dept;
   }
   public double getSalary() {
   	return salary;
   }
   public void display() {
	   System.out.println("print person details");
   }
    public void display(boolean dontPrintAge) {
    	System.out.println("prit person details without age");
    }
    public static void sayHello() {
    	System.out.println("hello person");
    }
 
    
}
