package collectionjava;
import java.util.Scanner;
public class ReadConsoleDataWithScannerEx {

	public static void main(String[] args) {
	  System.out.println("Enter details");
	  Scanner in=new Scanner(System.in);
	  System.out.println("Enter ur name");
	  String name=in.next();
	  System.out.println("enter ur age");
	  int age=in.nextInt();
	  System.out.println("enter ua designation");
	  String dsgn=in.next();
	  System.out.format("name \t age \t dsgn \n");
	  System.out.format("%s %d %s",name,age,dsgn);
	  in.close();

	}

}
