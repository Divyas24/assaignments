package collectionjava;




import java.util.Comparator;

import java.util.Set;

import java.util.TreeSet;



import corejava.Employ;



public class TreeSetEx {



	public static void main(String[] args) {



		// Employees are sorted by age

		Comparator EMPLOYEE_SORT_BY_AGE = new Comparator() {

			@Override

			public int compare(Object o1, Object o2) {

				if (o1 instanceof Employ && o2 instanceof Employ) {

					return ((Employ) o1).getAge() - ((Employ) o2).getAge();

				}

				return 0;

			}

		};



		Set employees = new TreeSet(EMPLOYEE_SORT_BY_AGE);



		employees.add(new Employ(100, 35, "IT", 55000));

		employees.add(new Employ(101, 25, "cs", 60000));

		employees.add(new Employ(102, 25, "is", 55000));

		employees.add(new Employ(103, 30, "ec", 58000));

		employees.add(new Employ(104, 28, "ee", 60000));

		employees.add(new Employ(105, 22, "ee", 50000));



		for (Object emp : employees) {

			System.out.println(emp);

		}

	}

}
