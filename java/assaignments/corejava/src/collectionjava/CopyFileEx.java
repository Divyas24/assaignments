package collectionjava;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class CopyFileEx {

	public static void main(String[] args) {
          FileInputStream in=null;
          FileOutputStream out=null;
          try {
        	  in=new FileInputStream("C:\\Users\\DivyaS\\Documents\\FS Training\\java\\corejava\\src\\collectionjava");
              out=new FileOutputStream("C:\\Users\\DivyaS\\Documents\\FS Training\\java\\corejava\\src\\collectionjava");
          int c;
          while((c=in.read())!=-1){
        	  System.out.println((char)c);
        	  out.write(c);
          }
          }catch(FileNotFoundException e) {
        	  System.out.println();
        	  e.printStackTrace();
          }
          
          

	}

}
